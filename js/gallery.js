
const ACTIVE_CLASS = "gallery-item-pic-active";
const ITEM_CLASS = "gallery-item-pic";

document.addEventListener("DOMContentLoaded", function(event){
    initGallery();
});


function initGallery()
{
    let galleryItemsDiv = document.querySelectorAll(".gallery-sub .gallery-item");
    let mainPicture = document.querySelector(".gallery-main-pic img");

    if(galleryItemsDiv.length != 0)
    {
        let firstPic = galleryItemsDiv[0].children[0];
        
        firstPic.className = ITEM_CLASS + " " + ACTIVE_CLASS;
        mainPicture.src = firstPic.src;

        for(i = 0; i < galleryItemsDiv.length; i++)
        {
            let accPic =  galleryItemsDiv[i].children[0];

            accPic.addEventListener("click",function(event){
                setGallery(event.target);
            });
        }
    }
}

function setGallery(element)
{
    let galleryItemsDiv = document.querySelectorAll(".gallery-sub .gallery-item");
    let mainPicture = document.querySelector(".gallery-main-pic img");

    if(galleryItemsDiv.length != 0)
    {     
        for(i = 0; i < galleryItemsDiv.length; i++)
        {
            let accPic =  galleryItemsDiv[i].children[0];

            accPic.className = ITEM_CLASS;
        }

        element.className = ITEM_CLASS + " " + ACTIVE_CLASS;
        mainPicture.src = element.src;
    }
}