
const ACTIVE_CLASS = "video-item-active";

document.addEventListener("DOMContentLoaded", function(event){
    initVideoGallery();
});


function initVideoGallery()
{
    let videoItems = document.querySelectorAll(".video-menu .video-item");
    let mainFrame = document.querySelector(".video-main iframe");

    if(videoItems.length != 0)
    {
        let firstVideo = videoItems[0].children[0];
        
        firstVideo.className = ACTIVE_CLASS;

        mainFrame.src = firstVideo.attributes.dataSrc.textContent;

        for(i = 0; i < videoItems.length; i++)
        {
            let accPic =  videoItems[i].children[0];

            accPic.addEventListener("click",function(event){
                setVideoGallery(event.target);
            });
        }
    }
}

function setVideoGallery(element)
{
    let videoItems = document.querySelectorAll(".video-menu .video-item");
    let mainFrame = document.querySelector(".video-main iframe");

    if(videoItems.length != 0)
    {     
        for(i = 0; i < videoItems.length; i++)
        {
            let accPic =  videoItems[i].children[0];

            accPic.className = "";
        }

        element.className = ACTIVE_CLASS;
        mainFrame.src = element.attributes.dataSrc.textContent
    }
}