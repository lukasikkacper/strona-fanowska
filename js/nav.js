document.addEventListener("DOMContentLoaded", function(event){
    initNav();
});


function initNav()
{
    document.querySelector(".toggle-nav").addEventListener("click",function(event){
        toggleMenu();
    });
}

function toggleMenu()
{
    let nav_element = document.querySelector('nav ul');
    let toggledButton = document.querySelector("nav .toggle-nav i");

    console.log(nav_element);

    if(nav_element.classList.contains("toggled-on"))
    {
        nav_element.className = "";
        toggledButton.className = "toggle-nav fas fa-align-justify";
    }
    else
    {
        nav_element.className = "toggled-on animated bounceInDown faster";
        toggledButton.className = "toggle-nav fas fa-times";
    }
        
}